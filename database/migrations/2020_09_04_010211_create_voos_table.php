<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVoosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_combinacao_aeroporto');
            $table->string('url');
            $table->dateTime('data_hora_partida');
            $table->dateTime('data_hora_chegada');
            $table->unsignedSmallInteger('velocidade_aproximada');
            $table->decimal('custo_tarifa_por_km', 10,2);
            $table->decimal('custo_tarifa_total', 10,2);
            $table->string('modelo_aeronave', 100);
            $table->timestamps();
        });

        Schema::table('voos', function($table) {
            $table->foreign('id_combinacao_aeroporto')->references('id')->on('combinacao_aeroportos')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voos');
    }
}
