<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateDuracaoVoosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE VIEW duracao_voos AS
                        SELECT
                          `v`.`id`           AS `id_voo`,
                          `a1`.`iata`           AS `iata_from`,
                          `a2`.`iata`           AS `iata_to`,
                          `c`.`distancia_km`    AS `distancia_km`,
                          `v`.`modelo_aeronave` AS `modelo_aeronave`,
                          TIMEDIFF(`v`.`data_hora_chegada`,`v`.`data_hora_partida`) AS `duracao`
                        FROM (((`voos` `v`
                             JOIN `combinacao_aeroportos` `c`
                               ON ((`c`.`id` = `v`.`id_combinacao_aeroporto`)))
                            JOIN `aeroportos` `a1`
                              ON ((`a1`.`id` = `c`.`id_aeroporto_from`)))
                           JOIN `aeroportos` `a2`
                             ON ((`a2`.`id` = `c`.`id_aeroporto_to`)))");
                            }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW duracao_voos");
    }
}
