<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCombinacaoAeroportosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('combinacao_aeroportos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_aeroporto_from');
            $table->unsignedBigInteger('id_aeroporto_to');
            $table->double('distancia_km');
            $table->timestamps();
        });

        Schema::table('combinacao_aeroportos', function($table) {
            $table->foreign('id_aeroporto_from')->references('id')->on('aeroportos')->onDelete('cascade');
            $table->foreign('id_aeroporto_to')->references('id')->on('aeroportos')->onDelete('cascade');
            $table->unique(['id_aeroporto_from', 'id_aeroporto_to']);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('combinacao_aeroportos');
    }
}
