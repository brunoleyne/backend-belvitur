### Teste Tecnico Belvitur

- PHP 7.4.9 

## Backend Instalação

- Após fazer download do projeto, utilizar composer para instalar dependências

```bash
$ composer install
```

- Adicionar um registro ao vhost, modificar Directory e DocumentRoot para diretorio correspondente ao Apache ou Nginx.
- DocumentRoot deve estar apontado para o diretório public.
- Editar arquivo .env inserindo credencials para o banco de dados.
- Rodar migrate utilizando o comando:
```bash
php artisan migrate 
```
- Popular tables utilizando os comandos (Código fonte disponível em app/Console/Commands):
```bash
php artisan update:aeroportos
php artisan update:voos
```
