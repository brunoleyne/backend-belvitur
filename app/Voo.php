<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Voo extends Model
{
    protected $fillable = [
        'id_combinacao_aeroporto',
        'url',
        'data_hora_partida',
        'data_hora_chegada',
        'velocidade_aproximada',
        'custo_tarifa_por_km',
        'custo_tarifa_total',
        'modelo_aeronave'
    ];

    public function combinacaoAeroporto()
    {
        return $this->belongsTo('App\CombinacaoAeroporto', 'id_combinacao_aeroporto', 'id');
    }
}
