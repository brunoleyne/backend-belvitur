<?php

namespace App\Http\Controllers;

use App\DuracaoVoo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RelatorioController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Relatório TOP 30 Viagens Longas
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTopViagensLongas()
    {
        $voos = DuracaoVoo::orderBy('distancia_km','DESC')->take(30)->get();

        return response()->json([
            'voos' => $voos
        ]);
    }

    /**
     * Relatório Aeroportos Quantidade Por Estado
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getQtdeAeroportosPorEstado()
    {
        $estados = DB::table('aeroportos')
            ->select(DB::raw('count(id) as quantidade, estado'))
            ->groupBy('estado')
            ->orderByDesc('quantidade')
            ->get();

        return response()->json([
            'estados_aeroportos' => $estados
        ]);
    }

    /**
     * Relatórios aeroportos de origem e qual o destino mais distante a ele
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function getVoosDistantes()
    {
        $voosDistantes = DB::select("
SELECT
  dv.iata_from AS origem,
  dv.iata_to AS destino,
  dv.distancia_km
FROM
  duracao_voos dv
  INNER JOIN
    (SELECT
      iata_from,
      MAX(distancia_km) AS distancia_km
    FROM
      duracao_voos
    GROUP BY iata_from) a
    ON a.iata_from = dv.iata_from
    AND a.distancia_km = dv.distancia_km
    GROUP BY dv.iata_from, dv.iata_to, dv.distancia_km
    ORDER BY origem
");

        return response()->json([
            'voos_distantes' => $voosDistantes,
        ]);
    }
    /**
     * Relatórios aeroportos de origem e qual o destino mais próximo a ele
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getVoosProximos()
    {
        $voosProximos = DB::select("
SELECT
  dv.iata_from AS origem,
  dv.iata_to AS destino,
  dv.distancia_km
FROM
  duracao_voos dv
  INNER JOIN
    (SELECT
      iata_from,
      MIN(distancia_km) AS distancia_km
    FROM
      duracao_voos
    GROUP BY iata_from) a
    ON a.iata_from = dv.iata_from
    AND a.distancia_km = dv.distancia_km
    GROUP BY dv.iata_from, dv.iata_to, dv.distancia_km
    ORDER BY origem
");

        return response()->json([
            'voos_proximos' => $voosProximos,
        ]);
    }
}
