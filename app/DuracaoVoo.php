<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DuracaoVoo extends Model
{
    protected $fillable = [
        'id',
        'iata_from',
        'iata_to',
        'distancia_km',
        'modelo_aeronave',
        'duracao'
    ];
}
