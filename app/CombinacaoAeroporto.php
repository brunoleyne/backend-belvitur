<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CombinacaoAeroporto extends Model
{
    protected $fillable = [
        'id_aeroporto_from',
        'id_aeroporto_to',
        'distancia_km'
    ];

    public function aeroportoFrom()
    {
        return $this->belongsTo('App\Aeroporto', 'id_aeroporto_from', 'id');
    }

    public function aeroportoTo()
    {
        return $this->belongsTo('App\Aeroporto', 'id_aeroporto_to', 'id');
    }
}
