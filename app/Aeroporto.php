<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aeroporto extends Model
{
    protected $fillable = [
        'iata',
        'cidade',
        'lat',
        'lon',
        'estado'
    ];
}
