<?php

namespace App\Console\Commands;

use App\Aeroporto;
use App\CombinacaoAeroporto;
use Illuminate\Console\Command;
use Haversini\Haversini;

class UpdateAeroportos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:aeroportos';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Atualiza Aeroportos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->line('Iniciando atualização de lista aeroportos');
        $client = new \GuzzleHttp\Client([
            'verify' => false,
            'auth' => [
                'demo',
                'swnvlD'
            ]
        ]);

        $requestJsonAeroportos = $client->get("http://stub.2xt.com.br/air/airports/pzrvlDwoCwlzrWJmOzviqvOWtm4dkvuc");
        $jsonAeroportos = json_decode($requestJsonAeroportos->getBody()->getContents(), true);

        foreach ($jsonAeroportos as $obj) {
            $aeroportoDB = Aeroporto::where(['iata' => $obj["iata"]])->first();
            if (is_null($aeroportoDB)) {
                Aeroporto::create([
                    'iata' => $obj["iata"],
                    'cidade' => $obj["city"],
                    'lat' => $obj["lat"],
                    'lon' => $obj["lon"],
                    'estado' => $obj["state"]

                ]);
            }
        }

        $aeroportoDBCollection = Aeroporto::all();
        foreach ($aeroportoDBCollection as $aeroportoFrom) {

            foreach ($aeroportoDBCollection as $aeroportoTo) {

                if ($aeroportoFrom == $aeroportoTo) // Não calcular de um mesmo aeroporto
                    continue;

                $checkAeroportoExist = CombinacaoAeroporto::where([
                    'id_aeroporto_from' => $aeroportoFrom->id,
                    'id_aeroporto_to' => $aeroportoTo->id,
                ])->first();

                if (is_null($checkAeroportoExist))
                {
                    $distanciaKm = Haversini::calculate(
                        $aeroportoFrom->lat,
                        $aeroportoFrom->lon,
                        $aeroportoTo->lat,
                        $aeroportoTo->lon
                    );

                    CombinacaoAeroporto::create([
                        'id_aeroporto_from' => $aeroportoFrom->id,
                        'id_aeroporto_to' => $aeroportoTo->id,
                        'distancia_km' => $distanciaKm
                    ]);
                }
            }
        }

        $this->info('Fim atualização Aeroportos.');

        return 0;
    }
}
