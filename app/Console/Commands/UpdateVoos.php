<?php

namespace App\Console\Commands;

use App\Aeroporto;
use App\Voo;
use App\CombinacaoAeroporto;
use Carbon\Carbon;
use Illuminate\Console\Command;

class UpdateVoos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:voos';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Atualiza Voos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $dataPlusQuarentaDias = Carbon::now()->addDays(40);
        $this->line("Iniciando atualização de lista de voos - {$dataPlusQuarentaDias->format('d/m/Y')}");
        $client = new \GuzzleHttp\Client([
            'verify' => false,
            'auth' => [
                'demo',
                'swnvlD'
            ]
        ]);

        $combinacoesAeroportoDBCollection = CombinacaoAeroporto::all();

        foreach ($combinacoesAeroportoDBCollection as $combinacao) {
            $url = "http://stub.2xt.com.br/air/search/pzrvlDwoCwlzrWJmOzviqvOWtm4dkvuc/{$combinacao->aeroportoFrom->iata}/{$combinacao->aeroportoTo->iata}/{$dataPlusQuarentaDias->format('Y-m-d')}";
            $requestJsonVoos = $client->get($url);
            $jsonVoos = json_decode($requestJsonVoos->getBody()->getContents(), true);

            foreach ($jsonVoos["options"] as $options) {
                $data_hora_partida = Carbon::parse($options["departure_time"]);
                $data_hora_chegada = Carbon::parse($options["arrival_time"]);

                $velocidade_aproximada = $combinacao->distancia_km/($data_hora_chegada->diffInMinutes($data_hora_partida));
                $velocidade_aproximada = $velocidade_aproximada * 60; // Transformar km/m em km/h
                $custo_tarifa_total = $options["fare_price"];
                $custo_tarifa_por_km = $custo_tarifa_total / $combinacao->distancia_km;
                $modelo_aeronave = "{$options["aircraft"]["manufacturer"]} - {$options["aircraft"]["model"]}";

                $checkVooExist = Voo::where([
                    'id_combinacao_aeroporto' => $combinacao->id,
                    'url' => $url,
                    'data_hora_partida' => $data_hora_partida,
                    'data_hora_chegada' => $data_hora_chegada,
                    'velocidade_aproximada' => $velocidade_aproximada,
                    'custo_tarifa_por_km' => $custo_tarifa_por_km,
                    'custo_tarifa_total' => $custo_tarifa_total,
                    'modelo_aeronave' => $modelo_aeronave,
                ])->first();

                if (is_null($checkVooExist))
                {
                    Voo::create([
                        'id_combinacao_aeroporto' => $combinacao->id,
                        'url' => $url,
                        'data_hora_partida' => $data_hora_partida,
                        'data_hora_chegada' => $data_hora_chegada,
                        'velocidade_aproximada' => $velocidade_aproximada,
                        'custo_tarifa_por_km' => $custo_tarifa_por_km,
                        'custo_tarifa_total' => $custo_tarifa_total,
                        'modelo_aeronave' => $modelo_aeronave,

                    ]);
                }
            }
        }

        $this->info('Fim atualização Voos.');
        return 0;
    }
}
